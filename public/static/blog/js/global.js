/**
 * Created by Administrator on 2017/9/8 0008.
 */
//一般直接写在一个js文件中
layui.use(['layer', 'form','element'], function(){
    var layer = layui.layer,
        form = layui.form,
        element = layui.element,
        $ = layui.$;
    $('#navicon').on('click',function (e) {
        $('.layui-nav-side').removeClass('layui-hide');
        layer.open({
            type: 1,
            shade: 0.1,
            title: false, //不显示标题
            area:['200px','100%'],
            offset:['60px','0px'],
            closeBtn:0,
            shadeClose:1,
            content: $('.layui-nav-side').show(), //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
            end: function(){
                $('.layui-nav-side').addClass('layui-hide');
            }
        });

    });
});