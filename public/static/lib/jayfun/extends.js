(function ($) {
    $.fn.extend({
        ////////////////////////////////////////////////////////////////
        /*图片本地预览*/
        'addPreview':function (id) {
            $(this).change(function(){
                var file=$(this)[0];
                if (file.files && file.files[0]){
                    var reader = new FileReader();
                    reader.onload = function(evt){

                        console.log(evt.total);
                        $("#"+id).attr('src', evt.target.result);
                    }
                    reader.readAsDataURL(file.files[0]);
                }
            });
        }
        /////////////////////////////////////////////////////////////
    })
})(jQuery)