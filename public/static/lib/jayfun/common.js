function var_dump(data) {
    console.log(data);
}

$(function () {
    /**
     * AJAX表单组件
     */
    $('.fa-ajax-form').on('submit',function (e) {
        e.preventDefault();
        var sub_btn = $(this).find(':submit');
        sub_btn.addClass('layui-btn-disabled').prop('disabled','disabled');
        var data = new FormData(this);
        var url = this.action;
        var method = this.method;
        $.ajax({
            url:url,
            type:method,
            processData: false,
            contentType: false,
            data: data,
            success:function (data) {
                layui.use(['layer'],function () {
                    var layer = layui.layer;
                    layer.msg(data.msg,{time:data.wait*1000},function () {
                        if (data.url.length>0){
                            location.href = data.url;
                        }
                        sub_btn.removeAttr('disabled').removeClass('layui-btn-disabled');
                    });
                })
            },
            error:function () {
                layui.use(['layer'],function () {
                    var layer = layui.layer;
                    layer.msg('网络异常，稍后重试！',{time:1000},function () {
                        sub_btn.removeAttr('disabled').removeClass('layui-btn-disabled');
                    });
                })
            }
        });
    })
})