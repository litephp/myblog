/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : thinkinfo

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-18 00:36:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for m_about
-- ----------------------------
DROP TABLE IF EXISTS `m_about`;
CREATE TABLE `m_about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `git_addr` varchar(255) NOT NULL,
  `composer` varchar(255) NOT NULL,
  `qq` char(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `info` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_about
-- ----------------------------
INSERT INTO `m_about` VALUES ('1', '弱鸡小码农', 'https://git.oschina.net/Mao02/', 'https://packagist.org/packages/jayfun/', '', 'sinan00@qq.com', '野生程序员，php入行，吃力的走在全栈的路上', 'php,javascript,java,python,mysql', '0', '1505021171', null);

-- ----------------------------
-- Table structure for m_admin
-- ----------------------------
DROP TABLE IF EXISTS `m_admin`;
CREATE TABLE `m_admin` (
  `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` char(40) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_admin
-- ----------------------------
INSERT INTO `m_admin` VALUES ('1', 'admin', '超管', '21232f297a57a5a743894a0e4a801fc3', '1', '0', '1504014794', '1505666082', null);

-- ----------------------------
-- Table structure for m_article
-- ----------------------------
DROP TABLE IF EXISTS `m_article`;
CREATE TABLE `m_article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `abstract` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `click` int(11) NOT NULL,
  `reply` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_article
-- ----------------------------

-- ----------------------------
-- Table structure for m_authrole
-- ----------------------------
DROP TABLE IF EXISTS `m_authrole`;
CREATE TABLE `m_authrole` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `access` text NOT NULL,
  `list_order` int(10) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_authrole
-- ----------------------------
INSERT INTO `m_authrole` VALUES ('1', '超级管理员', '拥有最高权限', '14,15,16,17,18,29,30,31,19,21,25,27,28,32,33,34,35,38,39,40,41,42,23,24,26', '50', '1504014771', '1505612678', null, '0');

-- ----------------------------
-- Table structure for m_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `m_auth_access`;
CREATE TABLE `m_auth_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL,
  `path` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `menu_group` tinyint(1) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `rule` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `param` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `list_order` int(11) NOT NULL,
  `is_show` tinyint(1) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_auth_access
-- ----------------------------
INSERT INTO `m_auth_access` VALUES ('14', '0', '', 'admin', '', '', '1', '', '', '系统', '', '后台顶部分组节点', '1', '50', '1', '0', '1504323217', '1504334825', null);
INSERT INTO `m_auth_access` VALUES ('15', '14', '', 'admin', '', '', '1', 'Hui-iconfont-user-zhanzhang', '', '管理员管理', '', '', '2', '50', '1', '0', '1504323361', '1504337578', null);
INSERT INTO `m_auth_access` VALUES ('16', '15', '', 'admin', 'admin', 'index', '0', 'Hui-iconfont-user', 'admin/admin/index', '管理员管理', '', '', '3', '50', '1', '0', '1504327612', '1504366316', null);
INSERT INTO `m_auth_access` VALUES ('17', '15', '', 'admin', 'auth_access', 'index', '0', 'Hui-iconfont-tags', 'admin/auth_access/index', '权限管理', '', '', '3', '50', '1', '0', '1504327631', '1504367382', null);
INSERT INTO `m_auth_access` VALUES ('18', '15', '', 'admin', 'authrole', 'index', '0', '', 'admin/authrole/index', '角色管理', '', '', '3', '50', '1', '0', '1504327660', '1504367507', null);
INSERT INTO `m_auth_access` VALUES ('19', '0', '', 'admin', '', '', '1', '', '', '博客', '', '', '1', '50', '1', '0', '1504327746', '1504330888', null);
INSERT INTO `m_auth_access` VALUES ('21', '19', '', 'admin', '', '', '1', '', '', '内容管理', '', '', '2', '50', '1', '0', '1504328034', '1504363334', null);
INSERT INTO `m_auth_access` VALUES ('22', '17', '', 'admin', 'auth_access', 'add', '0', '', 'admin/auth_access/add', '添加权限节点', '', '', '4', '50', '0', '0', '1504331223', '1504367413', null);
INSERT INTO `m_auth_access` VALUES ('25', '21', '', 'admin', 'blog.category', 'index', '0', '', 'admin/blog.category/index', '分类管理', '', '', '3', '50', '1', '0', '1504364110', '1504367544', null);
INSERT INTO `m_auth_access` VALUES ('27', '21', '', 'admin', 'blog.article', 'index', '0', '', 'admin/blog.article/index', '文章管理', '', '', '3', '50', '1', '0', '1504369624', '1504369624', null);
INSERT INTO `m_auth_access` VALUES ('28', '21', '', 'admin', 'blog.tags', 'index', '0', '', 'admin/blog.tags/index', '标签管理', '', '', '3', '50', '1', '0', '1504371019', '1504371019', null);
INSERT INTO `m_auth_access` VALUES ('29', '14', '', 'admin', '', '', '1', '', '', '友情链接', '', '', '2', '50', '1', '0', '1504371496', '1504371496', null);
INSERT INTO `m_auth_access` VALUES ('30', '29', '', 'admin', 'flink', 'index', '0', '', 'admin/flink/index', '友链审批', 'status=0', '', '3', '50', '1', '0', '1504371602', '1504423252', null);
INSERT INTO `m_auth_access` VALUES ('31', '29', '', 'admin', 'flink', 'index', '0', '', 'admin/flink/index', '有效链接', 'status=1', '', '3', '50', '1', '0', '1504371654', '1504708513', null);
INSERT INTO `m_auth_access` VALUES ('32', '19', '', 'admin', '', '', '1', '', '', '杂项管理', '', '', '2', '50', '1', '0', '1504710749', '1504710749', null);
INSERT INTO `m_auth_access` VALUES ('33', '32', '', 'admin', 'blog.speak', 'index', '0', '', 'admin/blog.speak/index', '说说管理', '', '', '3', '50', '1', '0', '1504710773', '1504710787', null);
INSERT INTO `m_auth_access` VALUES ('34', '32', '', 'admin', 'blog.project', 'index', '0', '', 'admin/blog.project/index', '项目管理', '', '', '3', '50', '1', '0', '1504711610', '1504711610', null);
INSERT INTO `m_auth_access` VALUES ('35', '32', '', 'admin', 'blog.about', 'edit', '0', '', 'admin/blog.about/edit', '关于我', 'id=1', '', '3', '50', '1', '0', '1504791632', '1504792454', null);
INSERT INTO `m_auth_access` VALUES ('38', '19', '', 'admin', '', '', '1', '', '', '互动管理', '', '', '2', '50', '1', '0', '1504793124', '1505527297', null);
INSERT INTO `m_auth_access` VALUES ('39', '38', '', 'admin', 'blog.guestbook', 'index', '0', '', 'admin/blog.guestbook/index', '留言审核', 'status=0', '', '3', '50', '1', '0', '1504793146', '1504793146', null);
INSERT INTO `m_auth_access` VALUES ('40', '38', '', 'admin', 'blog.guestbook', 'index', '0', '', 'admin/blog.guestbook/index', '展示留言', 'status=1', '', '3', '50', '1', '0', '1504793184', '1504793184', null);
INSERT INTO `m_auth_access` VALUES ('41', '38', '', 'admin', 'blog.comment', 'index', '0', '', 'admin/blog.comment/index', '评论审核', 'status=0', '', '3', '50', '1', '0', '1505527441', '1505527441', null);
INSERT INTO `m_auth_access` VALUES ('42', '38', '', 'admin', 'blog.comment', 'index', '0', '', 'admin/blog.comment/index', '文章评论', 'status=1', '', '3', '50', '1', '0', '1505527489', '1505527489', null);

-- ----------------------------
-- Table structure for m_category
-- ----------------------------
DROP TABLE IF EXISTS `m_category`;
CREATE TABLE `m_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `list_order` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_category
-- ----------------------------

-- ----------------------------
-- Table structure for m_comment
-- ----------------------------
DROP TABLE IF EXISTS `m_comment`;
CREATE TABLE `m_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_comment
-- ----------------------------

-- ----------------------------
-- Table structure for m_company
-- ----------------------------
DROP TABLE IF EXISTS `m_company`;
CREATE TABLE `m_company` (
  `company_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `lat` double(20,0) NOT NULL,
  `lng` double(20,0) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_company
-- ----------------------------

-- ----------------------------
-- Table structure for m_flink
-- ----------------------------
DROP TABLE IF EXISTS `m_flink`;
CREATE TABLE `m_flink` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `info` varchar(255) NOT NULL,
  `qq` char(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_flink
-- ----------------------------
INSERT INTO `m_flink` VALUES ('1', '百度', 'http://www.baidu.com', '百度一下，你就知道', '暂无', '', '1', '1504710233', '1505663242', null);

-- ----------------------------
-- Table structure for m_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `m_guestbook`;
CREATE TABLE `m_guestbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `qq` char(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for m_project
-- ----------------------------
DROP TABLE IF EXISTS `m_project`;
CREATE TABLE `m_project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `repository` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_project
-- ----------------------------
INSERT INTO `m_project` VALUES ('1', '基于layui的thinkphp5分页驱动', '\r\n# 基于layui的thinkphp5分页驱动\r\n\r\n## 安装\r\n\r\n```\r\ncomposer require jayfun/layui-pager\r\n```\r\n\r\n## 使用方法\r\n\r\n安装成功后修改thinkphp5配置文件`config.php`最底部的分页配置的type项\r\n\r\n```\r\n    //分页配置\r\n    \'paginate\'               =&gt; [\r\n        \'type\'      =&gt; \'jayfun\\layuiPager\\Layui\',\r\n        \'var_page\'  =&gt; \'page\',\r\n        \'list_rows\' =&gt; 3,\r\n    ],\r\n```\r\n\r\n大工告成，在layui为前端框架的模版中直接可以渲染出layui的分页按钮！！\r\n\r\n喜欢请star', 'https://git.oschina.net/Mao02/layuiPager', '1505524669', '1505574307', '0');
INSERT INTO `m_project` VALUES ('2', 'thinkphp5集成gatewayworker', '# thinkphp5集成gatewayworker \r\n\r\n该作品创建之初对gatewayworker以及thinkphp5了解未深，属于照搬手册中tp5与workerman集成方式，现有更好集成方案，等待博客上见', 'https://git.oschina.net/Mao02/thinkgate', '1504712366', '1505573442', '0');

-- ----------------------------
-- Table structure for m_speak
-- ----------------------------
DROP TABLE IF EXISTS `m_speak`;
CREATE TABLE `m_speak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `show` tinyint(1) unsigned NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_speak
-- ----------------------------
INSERT INTO `m_speak` VALUES ('1', '今天我的网站就快要完成了，谢谢大家的支持', '1', '1505577653', '1505577665', null);

-- ----------------------------
-- Table structure for m_tags
-- ----------------------------
DROP TABLE IF EXISTS `m_tags`;
CREATE TABLE `m_tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_tags
-- ----------------------------

-- ----------------------------
-- Table structure for m_tags_map
-- ----------------------------
DROP TABLE IF EXISTS `m_tags_map`;
CREATE TABLE `m_tags_map` (
  `tag_id` int(11) unsigned NOT NULL,
  `article_id` int(11) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_tags_map
-- ----------------------------

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` char(30) NOT NULL,
  `password` char(32) NOT NULL,
  `username` varchar(15) NOT NULL,
  `last_time` int(11) unsigned NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_user
-- ----------------------------
