<?php
namespace app\admin\model;
use traits\model\SoftDelete;

/**
 * 管理员模型
 * Class Admin
 * @package app\admin\model
 */
class Admin extends BaseModel {
    /*软删除*/
    use SoftDelete;
    /*删除字段*/
    protected $deleteTime = 'delete_time';
    /*角色列表*/
    protected static $rolelist = null;
    /*自动完成*/
    protected $insert = ['password'];

    /**
     * 自动完成密码
     * @param $value
     * @param $data
     * @param $relation
     * @return string
     */
    protected function setPasswordAttr($value, $data, $relation){
        return empty($value)?md5('admin001'):md5($value);
    }

    /**
     * 初始化
     */
    public static function init() {
        /*列表前置 id倒序*/
        static::event('before_list', function ($model) {
            $model->order('admin_id desc');
        });
        /*更新前，超管不能禁用*/
        static::event('before_update', function (&$model) {
            if(isset($model->repassword)) unset($model->repassword);
            if (!empty($model->status)){
                if ($model->admin_id == 1) {
                    $model->error = '初始管理员不能禁用！';
                    return false;
                }
            }
        });
        /*角色列表*/
        self::$rolelist = Authrole::where(function ($query){
            $query->where(['status'=>0]);
        })->column('name','id');
    }

    /**
     * 禁用状态修改器
     * @param $value
     * @param $data
     * @param $relation
     * @return mixed|null
     */
    public function getStatusAttr($value, $data ,$relation){
        $status = [1=>'禁用',0=>'正常'];
        return isset($status[$value])?$status[$value]:null;
    }

    /**
     * 角色ID修改器
     * @param $value
     * @param $data
     * @param $relation
     * @return mixed
     */
    public function getRoleIdAttr($value, $data ,$relation){
        return self::$rolelist[$value];
    }

    /**
     * 关联模型
     * @return \think\model\relation\HasOne
     */
    public function authrole() {
        $aa = $this->hasOne('Authrole','id','admin_id');
        return $aa;
    }
}