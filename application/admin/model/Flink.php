<?php
namespace app\admin\model;
/**
 * 友情链接模型
 * Class Flink
 * @package app\admin\model
 */
class Flink extends BaseModel {
    /*自动完成*/
    protected $insert = ['status'];

    /**
     * 初始化
     */
    public static function init() {
        /*列表前置*/
        static::event('before_list',function($model){
            $model->where(request()->route());
        });
    }

    /**
     * 设置状态
     * @return string
     */
    public function setStatusAttr(){
        return '1';
    }

    /**
     * 自动获取QQ
     * @param $value
     * @param $data
     * @return string
     */
    public function getQqAttr($value,$data){
        return $value?:'暂无';
    }

    /**
     * 自动获得邮箱
     * @param $value
     * @param $data
     * @return string
     */
    public function getEmailAttr($value,$data){
        return $value?:'暂无';
    }
}