<?php
namespace app\admin\model;
/**
 * 权限节点模型
 * Class AuthAccess
 * @package app\admin\model
 */
class AuthAccess extends BaseModel {
    /*是否分页*/
    protected $isMultiPage = false;
    /*父类列表*/
    protected static $pidlist = null;
    /*自动完成*/
    protected $insert = [];
    //protected $update = ['module'=>'admin','rule'];
    protected $auto = ['module'=>'admin','rule','is_show', 'level'];

    /**
     * 权限规则自动完成
     * @param $value
     * @param $data
     * @param $relation
     * @return string
     */
    protected function setRuleAttr($value, $data, $relation){
        if (notEmpty($data['action']) && notEmpty($data['controller']) && notEmpty($data['module'])){
            return $data['module'].'/'.$data['controller'].'/'.$data['action'];
        }else{
            return '';
        }
    }

    /**
     * 自动设置等级
     * @param $value
     * @param $data
     * @param $relation
     * @return int|mixed
     */
    protected function setLevelAttr($value, $data, $relation){
        $plevel = $this->where('id',$data['pid'])->value('level');
        return empty($plevel)?1:$plevel+1;
    }

    /**
     * 自动设置显示状态
     * @param $value
     * @param $data
     * @param $relation
     * @return int
     */
    protected function setIsShowAttr($value, $data, $relation){
        return empty($value)?0:$value;
    }

    /**
     * 初始化
     */
    public static function init() {
        /*列表前置 设置排序规则*/
        static::event('before_list', function ($model) {
            $model->order('level,id');
        });
        /*列表后置 补充状态字段*/
        static::event('after_list', function (&$list){
            foreach ($list as $item){
                $item->status_data = $item->getData('status');
            }
            array_to_tree_list($list);
        });
        /*插入前置*/
        static::event('before_insert', function (&$model){
            if ($model->getData('rule') === ''){
                $has = null;
            }else {
                $has = $model->where([
                    'rule' => $model->getData('rule'),
                    'param' => $model->param
                ])->value('id');
            }
            if (!empty($has)){
                $model->error = '存在相同规则节点！';
                return false;
            }
            if ($model->getData('level')>4){
                $model->error = '四级节点不能添加子节点！';
                return false;
            };
        });
        /*更新前置*/
        static::event('before_update', function (&$model){
            if ($model->getData('rule') === ''){
                $has = null;
            }else{
                $has =$model->where([
                    'rule'=>$model->getData('rule'),
                    'id'=>['<>',$model->getData('id')],
                    'param' => $model->param
                ])->value('id');
            }
            if (!empty($has)){
                $model->error = '存在相同规则节点！';
                return false;
            }
            if ($model->getData('level')>4){
                $model->error = '四级节点不能添加子节点！';
                return false;
            };
        });
        /*删除前置*/
        static::event('before_delete', function (&$model){
            if ($model::get(['pid'=>$model->id])){
                $model->error = '存在子节点不得删除';
                return false;
            }
        });
        /*父类列表*/
        self::$pidlist = self::where(function ($query){
            $query->where(['level'=>['<=',3]]);
        })->column('name','id');
        self::$pidlist[0]='顶级节点';
    }

    /**
     * 获取状态
     * @param $value
     * @param $data
     * @param $relation
     * @return mixed
     */
    public function getStatusAttr($value, $data ,$relation){
        $status = [1=>'禁用',0=>'正常'];
        return $status[$value];
    }

    /**
     * 获取父节点
     * @param $value
     * @param $data
     * @param $relation
     * @return mixed
     */
    public function getPidNameAttr($value, $data ,$relation){
        return self::$pidlist[$data['pid']];
    }
}