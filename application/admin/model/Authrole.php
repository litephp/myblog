<?php
namespace app\admin\model;
use traits\model\SoftDelete;

/**
 * 后台角色模型
 * Class Authrole
 * @package app\admin\model
 */
class Authrole extends BaseModel {
    /**
     * 初始化
     */
    public static function init() {
        /*列表前置*/
        static::event('before_list', function ($model) {
            $model->where(function ($query){
                $query->where(['id'=>['<',100]]);
            })->order('id desc');
        });
    }

    /**
     * 获取状态字段
     * @param $value
     * @param $data
     * @param $relation
     * @return mixed
     */
    public function getStatusAttr($value, $data ,$relation){
        $status = [1=>'禁用',0=>'正常'];
        return $status[$value];
    }
}