<?php
namespace app\admin\model\blog;
use app\admin\model\BaseModel;

/**
 * 标签模型
 * Class Tags
 * @package app\admin\model\blog
 */
class Tags extends BaseModel {
    /**
     * 获取文章数量
     * @param $value
     * @param $data
     * @return int|string
     */
    public function getArtNumAttr($value,$data) {
        return db('TagsMap')->where(['tag_id'=>$data['tag_id']])->count();
    }

    /**
     * 初始化
     */
    public static function init() {
        /*插入前置 标签存在直接返回编号*/
        static::event('before_insert',function($model){
            $has_tag = static::get(['name'=>$model->getData('name')]);
            if ($has_tag){
                $model->tag_id = $has_tag->tag_id;
                return false;
            }
        });
    }
}