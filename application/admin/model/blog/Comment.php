<?php
namespace app\admin\model\blog;
use app\admin\model\BaseModel;

/**
 * 文章评论模型
 * Class Comment
 * @package app\admin\model\blog
 */
class Comment extends BaseModel {
    /**
     * 初始化
     */
    public static function init() {
		/*列表前置 TODO 当转为多用户时 需增强安全策略*/
		static::event('before_list',function($model){
			$model->where(request()->route());
		});
		/*文章评论数同步增加*/
		static::event('before_update',function($model){
			$article = Article::get($model->article_id);
			$article->setInc('reply',1);
		});
		/*文章评论数同步减少*/
		static::event('before_delete',function($model){
			if ($model->status){
				$article = Article::get($model->article_id);
				$article->setInc('reply',-1);
			}
		});
    }
}