<?php
namespace app\admin\model\blog;
use app\admin\model\BaseModel;

/**
 * 留言板模型
 * Class Guestbook
 * @package app\admin\model\blog
 */
class Guestbook extends BaseModel {
    /**
     * 初始化
     */
    public static function init() {
        /*列表前置 TODO 当转为多用户时 需增强安全策略*/
        static::event('before_list',function($model){
            $model->where(request()->route());
        });
    }
}