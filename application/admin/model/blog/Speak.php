<?php
namespace app\admin\model\blog;
use app\admin\model\BaseModel;

/**
 * 说说模型
 * Class Speak
 * @package app\admin\model\blog
 */
class Speak extends BaseModel {
    /**
     * 获取显示状态
     * @param $value
     * @param $data
     * @return mixed
     */
    public function getShowAttr($value, $data) {
        $show = [0=>'隐藏',1=>'显示'];
        return $show[$value];
    }
}