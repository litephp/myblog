<?php
namespace app\admin\model;
use think\Model;

/**
 * 后台基础模型
 * Class BaseModel
 * @package app\admin\model
 */
class BaseModel extends Model {
    /*多页*/
    protected $isMultiPage = true;

    /**
     * 获得列表数据
     * 增加了list方法的前后置事件
     * @param $request
     * @return false|\PDOStatement|string|\think\Collection|\think\Paginator
     */
    public function getIndexList($request) {
        $this->trigger('before_list', $this);//勾起自定义事件
        if ($this->isMultiPage){
            $list = $this->order($this->getPk().' desc')->paginate(5);
        }else{
            $list = $this->select();
        }
        $this->trigger('after_list', $list);//勾起自定义事件
        return $list;
    }

    /**
     * 插入数据
     * @param $post_data
     * @return false|int
     */
    public function insertData($post_data) {
        $vali_class = str_replace('model','validate',get_class($this));
        return $this->validate($vali_class.'.insert')->save($post_data);
    }

    /**
     * 更新数据
     * @param $post_data
     * @return false|int
     */
    public function updateData($post_data) {
        $vali_class = str_replace('model','validate',get_class($this));
        return $this->validate($vali_class.'.update')->isUpdate(true)->save($post_data);
    }

    /**
     * 快速开关
     * @param $switch_data
     * @param $id
     * @return false|int
     */
    public function switchData($switch_data, $id){
        $key = key($switch_data);
        $value = abs(current($switch_data));

        $target_data = $this::get($id);
        $target_data->$key = $value;

        $res = $target_data->save();
        if ($res === false){
            $this->error = $target_data->getError();
        }
        return $res;
    }

    /**
     * 删除数据
     * @param $data
     * @return int
     */
    public function deleteData($data) {
        return $this->where($data)->delete();
    }
}