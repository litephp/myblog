<?php
namespace app\admin\validate;
use think\Validate;

/**
 * 权限节点验证器
 * Class AuthAccess
 * @package app\admin\validate
 */
class AuthAccess extends Validate {
    protected $message  =   [
        'level.max' => '四级节点不允许添加子节点',
    ];
    protected $scene = [
        'insert'  =>  ['level', 'pid', 'controller', 'action', 'name',  'list_order'],
        'update'  =>  ['level', 'id', 'pid', 'controller', 'action', 'name', 'list_order']
    ];
    protected $rule = [
        'level'  =>  'max:3',
        'id'=>'require|number|egt:0',
        'pid|父节点'=>'require|number|egt:0',
        'module|模块'=>'requireIf:menu_group,0',
        'controller|控制器'=>'requireIf:menu_group,0',
        'action|操作'=>'requireIf:menu_group,0',
        'name|名称'  =>  'require',
        'remark|备注'  =>  'require',
        'list_order|排序' =>  'require|number',
        'is_show|是否可见'  =>  'require|number|in:0,1',
    ];
}