<?php
namespace app\admin\validate;
use think\Validate;

/**
 * 管理员验证器
 * Class Admin
 * @package app\admin\validate
 */
class Admin extends Validate {
    protected $message  =   [
        'level.max' => '四级节点不允许添加子节点',
    ];
    protected $scene = [
        'update'  =>  ['repassword']
    ];
    protected $rule = [
        'repassword|确认密码'=>'requireWith:password|confirm:password'
    ];
}