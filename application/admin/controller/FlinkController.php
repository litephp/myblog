<?php
namespace app\admin\controller;
use app\admin\controller\base\BaseController;

/**
 * 友情链接控制器
 * Class FlinkController
 * @package app\admin\controller
 */
class FlinkController extends BaseController {
    /*列表页跳转参数*/
    protected $indexParam = ['status'=>1];
}