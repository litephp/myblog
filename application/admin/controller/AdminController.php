<?php
namespace app\admin\controller;
use app\admin\controller\base\BaseController;
use think\Loader;
use think\Request;

/**
 * 管理员控制器
 * Class AdminController
 * @package app\admin\controller
 */
class AdminController extends BaseController {
    /**
     * 修改密码
     * @param Request $request
     * @return mixed
     */
    public function repassword(Request $request){
        if ($request->isPost()){
            $post_data = $request->post();
            $model = Loader::model('Admin');
            $res = $model->updateData($post_data);
            if ($res !== false){
                $this->success('分配权限成功！',url('index'));
            }else{
                $this->error('分配权限失败！'.$model->getError());
            }
        }else{
            $id = $request->param('id');
            $this->assign('admin_id',$id);
            return $this->fetch();
        }


    }
}