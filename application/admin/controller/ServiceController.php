<?php
namespace app\admin\controller;
use app\admin\logic\Admin;
use think\Controller;
use think\Loader;
use think\Request;
use think\Session;

/**
 * 公开服务控制器
 * Class ServiceController
 * @package app\admin\controller
 */
class ServiceController extends Controller {
    /**
     * 登录页面及操作
     * @param Request $request
     * @return mixed
     */
    public function login(Request $request) {
        if (Session::has('admin_id')) $this->redirect('admin/index/index');
        if ($request->isPost()){
            $account = $request->post('account');
            $password = $request->post('password');
            $verify = $request->post('verify');
            if(!captcha_check($verify)){
                $this->error('请输入正确的验证码！');
            };
            $logic = new Admin();
            if (!$logic->login($account, $password)){
                $this->error($logic->getError());
            }
            $this->success('登录成功！',url('admin/index/index'));
        }else{
            return $this->fetch();
        }
    }
}