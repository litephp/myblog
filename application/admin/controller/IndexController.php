<?php
namespace app\admin\controller;
use app\admin\controller\base\BaseController;
use app\admin\logic\Admin;
use app\admin\logic\Menu;
use think\Loader;
use think\Request;
use think\Session;

/**
 * 首页控制器
 * Class IndexController
 * @package app\index\controller
 */
class IndexController extends BaseController{
    /**
     * 后台首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        Session::clear('think');
        $menu_logic = new Menu();
        $menus = $menu_logic->getTree();
        $this->assign('menus',$menus);
        return $this->fetch();
    }

    /**
     * 欢迎页
     * TODO 集成仪表盘功能
     * @return string
     */
    public function welcome() {
		return $this->fetch();
    }

    /**
     * 推出登陆
     */
    public function logout(){
        $logic = new Admin();
        $logic->logout();
        $this->success('退出登录成功！');
    }
}
