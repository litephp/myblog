<?php
namespace app\admin\controller\get;
use app\admin\model\Authrole;
use app\admin\model\blog\Category;
use think\Controller;
use think\Hook;
use think\Loader;
use think\Request;
use think\Response;

/**
 * get请求API控制器
 * Class ApiController
 * @package app\admin\controller\get
 */
class ApiController extends Controller {
    /*控制器文件夹路径*/
    protected $ctl_path;

    /**
     * 初始化
     */
    public function _initialize() {
        Hook::listen('admin_api_init');/*TODO api 访问次数限制*/
        $this->ctl_path = APP_PATH.'admin'.DS.'controller'.DS;
    }

    /**
     * 获取后台控制器
     * @return Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function controllers(Request $request) {
        $controller = $request->param('controller');
        if ($index = strpos($controller,'.')){
            $arr = explode('.',$controller);
            $arr[1] = Loader::parseName($arr[1],1);
            $controller = implode('.',$arr);
        }else{
            $controller = Loader::parseName($controller,1);
        }
        clearstatcache();
        $ctl = '<option value="">请选择控制器</option>';
        if (!$fb = opendir($this->ctl_path)){
            $this->error('目录读取失败！');
        }
        while ($file = readdir($fb)){
            if (in_array($file,['.','..'])) continue;
            $subpath = $this->ctl_path.DS.$file;
            if (is_dir($subpath)){
                $ctl .= '<optgroup label="'.$file.'">';
                $subdir = opendir($subpath);
                while ($subfile = readdir($subdir)){
                    if (in_array($subfile,['.','..'])) continue;
                    $select = ($controller.'Controller.php' == $file.'.'.$subfile)?'selected':'';
                    $ctl .= '<option value="'.$file.'.'.Loader::parseName($subfile,0).'" '.$select.'>'.$file.'.'.$subfile.'</option>';
                }
                $ctl .= '</optgroup>';
            }else{
                $select = ($controller.'Controller.php' == $file)?'selected':'';
                $ctl .= '<option value="'.Loader::parseName($file,0).'" '.$select.'>'.$file.'</option>';
            }
        }
        $ctl = str_replace('_controller.php','',$ctl);
        return Response::create($ctl,'html');
    }

    /**
     * 获取后台控制器对应方法
     * @param Request $request
     * @return Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function actions(Request $request){
        $controller = $request->param('controller');
        if ($index = strpos($controller,'.')){
            $arr = explode('.',$controller);
            $arr[1] = Loader::parseName($arr[1],1);
            $controller = implode('.',$arr);
        }else{
            $controller = Loader::parseName($controller,1);
        }
        $action = $request->param('action');
        if ($controller === 'undefined'){
            return Response::create('<option value="">请选择操作</option>','html');
        }
        clearstatcache();
        $ctl = str_replace('.','\\',$controller);

        $thinkclass = 'think\\controller';
        $thinkmethod = get_class_methods($thinkclass);

        $class = 'app\\admin\\controller\\'.$ctl.'Controller';
        $method = get_class_methods($class);

        $act = '<option value="">请选择操作</option>';
        if (empty($method)){
            return Response::create($act,'html');
        }
        foreach ($method as $item){
            if (in_array($item,$thinkmethod)) continue;
            $select = ($action == $item)?'selected':'';
            $act .= '<option value="'.$item.'" '.$select.'>'.$item.'</option>';
        }

        return Response::create($act,'html');
    }

    /**
     * 获取角色
     * @param Request $request
     * @return Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function roles(Request $request) {
        $role_id = $request->param('role_id');

        $list = Authrole::all(['status'=>0]);
        $role = '';
        foreach ($list as $item){
            $select = ($role_id == $item['id'])?'selected':'';
            $role .= '<option value="'.$item['id'].'" '.$select.'>'.$item['name'].'</option>';
        }
        return Response::create($role,'html');

    }

    /**
     * 获取分类
     * @return Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function categorys() {
        $list = Category::all();
        $cats = '<option value="0" >请选择分类</option>';
        foreach ($list as $item){
            $cats .= '<option value="'.$item['category_id'].'" >'.$item['name'].'</option>';
        }
        return Response::create($cats,'html');
    }
    
    /*public function parentnode(Request $request) {
        $id = $request->param('id');
        $pid = $request->param('pid');
        $model = Loader::model('AuthAccess');
        $parent = $model->where('id',$pid)->column('name','id');
        $parent_node = '<option value="">顶级节点</option>';
        if (empty($parent_node)){
            return Response::create($parent_node,'html');
        }
        foreach ($parent as$key => $value){
            $parent_node .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return Response::create($parent_node,'html');
    }*/
}