<?php
namespace app\admin\controller\get;
use app\admin\model\Authrole;
use app\admin\model\blog\Article;
use app\admin\model\blog\Category;
use app\admin\model\blog\Comment;
use app\admin\model\blog\Guestbook;
use app\admin\model\blog\Tags;
use think\Controller;
use think\exception\HttpResponseException;
use think\Hook;
use think\Loader;
use think\Request;
use think\Response;
use think\Session;

/**
 * 统计请求API控制器
 * Class IndexApiController
 * @package app\admin\controller\blog
 */
class IndexApiController extends Controller {
	/**
	 * 初始化时校验登录
	 */
	protected function _initialize() {
		if(!Session::has('admin_id')){
			throw new HttpResponseException(Response::create('请登录！','html'));
		}
	}

	/**
	 * 文章数
	 * @return int|string
	 */
	public function getArtNum() {
		return Article::count();
	}

	/**
	 * 分类数
	 * @return int|string
	 */
	public function getCateNum() {
		return Category::count();
	}

	/**
	 * 标签数
	 * @return int|string
	 */
	public function getTagsNum() {
		return Tags::count();
	}

	/**
	 * 留言数
	 * @return int|string
	 */
	public function getGbNum() {
		return Guestbook::where(['status'=>1])->count();
	}

	/**
	 * 未审留言数
	 * @return int|string
	 */
	public function getGbunNum() {
		return Guestbook::where(['status'=>0])->count();
	}

	/**
	 * 评论数
	 * @return int|string
	 */
	public function getCmNum() {
		return Comment::where(['status'=>1])->count();
	}

	/**
	 * 未审评论数
	 * @return int|string
	 */
	public function getCmunNum() {
		return Comment::where(['status'=>0])->count();
	}
}