<?php
namespace app\admin\controller\blog;
use app\admin\controller\base\BaseController;
use think\Loader;
use think\Request;

/**
 * 关于我控制器
 * Class AboutController
 * @package app\admin\controller\blog
 */
class AboutController extends BaseController {
    /**
     * 重写编辑与提交操作
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        if ($request->isPost()){
            $post_data = $request->post();
            $model = Loader::model($this->modelname);
            $res = $model->updateData($post_data);
            if ($res !== false){
                $this->success('修改数据成功！',url('edit',['id'=>1]));
            }else{
                $this->error('修改数据失败！'.$model->getError());
            }
        }else{
            $id = $request->param('id');
            $model = Loader::model($this->modelname);
            $target_data = $model::get($id);
            $this->assign('item',$target_data);
            return $this->fetch();
        }
    }
}