<?php
namespace app\admin\controller\blog;
use app\admin\controller\base\BaseController;

/**
 * 留言板控制器
 * Class GuestbookController
 * @package app\admin\controller\blog
 */
class GuestbookController extends BaseController {
    /*列表页跳转参数*/
    protected $indexParam = ['status'=>0];
}