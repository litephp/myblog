<?php
namespace app\admin\controller\blog;
use app\admin\controller\base\BaseController;

/**
 * 文章评论控制器
 * Class CommentController
 * @package app\admin\controller\blog
 */
class CommentController extends BaseController {
    /*列表页跳转参数*/
    protected $indexParam = ['status'=>0];
}