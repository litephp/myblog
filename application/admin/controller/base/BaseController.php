<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/5 0005
 * Time: 下午 13:41
 */

namespace app\admin\controller\base;
use think\Config;
use think\Controller;
use think\Loader;
use think\Request;

/**
 * 基础控制器
 * Class BaseController
 * @package app\index\controller\base
 */
class BaseController extends Controller {
    protected $indexParam = [];

    /**
     * 初始化
     */
    public function _initialize() {
        $request = Request::instance();
        $website = Config::get('website');
        $this->assign('cfg_website',$website['sitename']);
        $this->modelname = ucfirst($request->controller());
    }

    /**
     * 列表页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request) {
        $model = Loader::model($this->modelname);
        $list = $model->getIndexList($request);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 添加页面及提交
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request){
        if ($request->isPost()){
            $post_data = $request->post();
            $model = Loader::model($this->modelname);
            $res = $model->insertData($post_data);
            if ($res !== false){
                $this->success('添加数据成功！',url('index',$this->indexParam));
            }else{
                $this->error('添加数据失败！'.$model->getError());
            }
        }else{
            return $this->fetch();
        }
    }

    /**
     * 编辑页面及提交
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){
        if ($request->isPost()){
            $post_data = $request->post();
            $model = Loader::model($this->modelname);
            $res = $model->updateData($post_data);
            if ($res !== false){
                $this->success('修改数据成功！',url('index',$this->indexParam));
            }else{
                $this->error('修改数据失败！'.$model->getError());
            }
        }else{
            $id = $request->param('id');
            $model = Loader::model($this->modelname);
            $target_data = $model::get($id);
            $this->assign('item',$target_data);
            return $this->fetch();
        }
    }

    /**
     * 删除操作
     * @param Request $request
     */
    public function delete(Request $request){
        $id = $request->param('id');
        $model = Loader::model($this->modelname);
        $target_data = $model::get($id);
        $res = $target_data->delete();
        if ($res !== false){
            $this->success('删除数据成功！',url('index',$this->indexParam));
        }else{
            $this->error('删除数据失败！'.$target_data->getError());
        }
    }

    /**
     * 批量删除
     * @param Request $request
     */
    public function delall(Request $request) {
        $ids = $request->post('check_ids/a');
        $model = Loader::model($this->modelname);
        foreach ($ids as $id){
            $target_data = $model::get($id);
            $target_data->delete();
        }
        $this->success('删除数据成功！',url('index',$this->indexParam));
    }

    /**
     * 状态开关
     * @param Request $request
     */
    public function switchStatus(Request $request) {
        $id = $request->param('id');
        $param_data = $request->except('id','param');

        $model = Loader::model($this->modelname);
        $res = $model->switchData($param_data,$id);
        if ($res !== false){
            $this->success('切换状态成功！');
        }else{
            $this->error('切换状态失败！'.$model->getError());
        }
    }
}