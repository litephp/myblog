<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/2 0002
 * Time: 下午 16:15
 */
return [
    'session' => [
        'id' => '',
        'var_session_id' => '',
        'prefix' => 'admin',
        'type' => '',
        'auto_start' => true,
    ],
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => true,
];
