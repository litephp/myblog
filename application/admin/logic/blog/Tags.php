<?php
namespace app\admin\logic\blog;
use app\admin\model\blog\Tags as TagsModel;

/**
 * 标签逻辑
 * Class Tags
 * @package app\admin\logic\blog
 */
class Tags {
    /**
     * 插入标签名返回标签id列表
     * @param $tagstr
     * @return string
     */
    public function insertTag($tagstr){
    	if(empty($tagstr)) return '';
        $tagarr = explode(',',$tagstr);
        $tag_ids = [];
        foreach ($tagarr as $item){
            if (empty($item)) continue;
            $tag = new TagsModel(['name'=>$item]);
            $tag ->save() ;
            $tag_ids[] = $tag->tag_id;
        }
        return implode(',',$tag_ids);
    }
}