<?php
namespace app\admin\logic;
use think\Hook;
use think\Session;

/**
 * 后台管理员逻辑
 * Class Admin
 * @package app\admin\logic
 */
class Admin extends BaseLogic {
    /**
     * 登录逻辑
     * @param $account
     * @param $password
     * @return bool
     */
    public function login($account,$password) {
        $admin = \app\admin\model\Admin::get(['account'=>$account,'password'=>md5($password)]);
        if (!$admin){
            $this->error = "请输入正确的账号密码!";
            return false;
        }
        if ($admin->getData('status') == 1){
            $this->error = "用户账号已被禁用";
            return false;
        }
        Session::set('admin_id',$admin->admin_id);
        Session::set('admin_name',$admin->name);
        Session::set('role_id',$admin->getData('role_id'));
        Session::set('role',$admin->role_id);
        Session::set('access',$admin->authrole->access);
        Hook::listen('admin_login_end');
        return true;
    }

    /**
     * 退出逻辑
     * @return bool
     */
    public function logout(){
        Session::clear();
        return true;
    }
}