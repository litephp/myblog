<?php
namespace app\admin\logic;
use app\admin\model\AuthAccess;

/**
 * 菜单相关逻辑
 * Class Menu
 * @package app\admin\logic
 */
class Menu extends BaseLogic {
    /**
     * 获取菜单树
     * @param int $pid
     * @return array
     */
    public function getTree($pid = 0){
        $list = AuthAccess::all([
           'is_show' => 1,
            'status' => 0,
            'level' => ['<=', 3]
        ]);
        $tree = array_to_tree($list);
        return $tree;
    }
}