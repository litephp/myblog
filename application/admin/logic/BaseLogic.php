<?php
namespace app\admin\logic;
/**
 * 基础逻辑
 * Class BaseLogic
 * @package app\admin\logic
 */
class BaseLogic {
    /*错误信息*/
    protected $error;

    /**
     * 获取出错误信息
     * @return mixed
     */
    public function getError(){
        return $this->error;
    }
}