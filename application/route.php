<?php
return [
    'index'=>'index/index/index',
    'article/:article_id'=>'index/article/index',
    'category'=>'index/category/index',
    'category/:cate_id'=>'index/category/index',
    'tags'=>'index/tags/index',
    'tags/:tag_id'=>'index/tags/index',
    'speak'=>'index/speak/index',
    'project'=>'index/project/index',
    'guestbook'=>'index/guestbook/index',
    'flink'=>'index/flink/index',
    'about'=>'index/about/index',
    'comment_insert'=>'index/comment/insert',
    'guestbook_insert'=>'index/guestbook/insert',
];
