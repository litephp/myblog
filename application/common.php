<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * 数组整理成树形
 * @param $array
 */
function array_to_tree_list(&$array){
    $tree = array_to_tree($array);
    $array = [];
    tree_to_array($tree, $array);
    return $array;
}
function array_to_tree($array){
    $list = [];
    $tree = [];
    foreach ($array as $item){
        $item->data('pid_name', $item->pid_name) ;
        $list[$item['id']] = $item->toArray();
    }
    foreach ($list as &$val){
        if (isset($list[$val['pid']])){
            $list[$val['pid']]['child'][] = &$val;
        }elseif(intval($val['pid']) == 0){
            $tree[] = &$val;
        }
    }
    return $tree;
}
function tree_to_array($tree, &$array){
    foreach ($tree as &$item){
        $array[] = &$item;
        if (isset($item['child'])){
            tree_to_array($item['child'],$array);
        }
        unset($item['child']);
    }
}
function notEmpty(&$param){
    return isset($param) && !empty($param);
}
