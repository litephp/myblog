<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/5 0005
 * Time: 下午 13:41
 */

namespace app\index\controller\base;
use think\Config;
use think\Controller;

/**
 * 基础控制器
 * Class BaseController
 * @package app\index\controller\base
 */
class BaseController extends Controller {
    public function _initialize() {
        $website = Config::get('website');
        $this->assign('website',$website);
    }
}