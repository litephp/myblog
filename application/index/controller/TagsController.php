<?php
namespace app\index\controller;
use app\index\controller\base\BaseController;
use app\index\model\Tags;

class TagsController extends BaseController {
    public function index() {
    	if (!request()->has('tag_id','route')){
			$tag = ['name'=>'暂无标签'];
		} else {
			$tag = Tags::get(request()->only('tag_id','route'));
		}
		$this->assign('tag', $tag);
        return $this->fetch();
    }
}