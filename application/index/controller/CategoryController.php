<?php
namespace app\index\controller;
use app\index\controller\base\BaseController;
use app\index\model\Category;

class CategoryController extends BaseController {
    public function index(){
        $cate_now= [];
        if (!request()->has('cate_id','route')){
            $map = ['category_id'=>request()->route('cate_id')];
            $cate_now = Category::where($map)->find();
        }
        $this->assign('cate_now',$cate_now);
        return $this->fetch();
    }
}