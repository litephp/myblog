<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/9 0009
 * Time: 下午 23:48
 */

namespace app\index\controller;
use app\index\controller\base\BaseController;

class ProjectController extends BaseController {
    public function index(){
        return $this->fetch();
    }
}