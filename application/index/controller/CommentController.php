<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10 0010
 * Time: 下午 12:43
 */

namespace app\index\controller;
use app\index\controller\base\BaseController;
use app\index\model\Comment;
use think\Hook;
use think\Request;

class CommentController extends BaseController {
    public function insert(Request $request) {
		$param = $request->only(['article_id','link','content'],'post');
		$param = $request->post();
        $comment = new Comment();
        $res = $comment->validate('Comment.user_insert')->allowField(true)->save($param);
        if ($res !== false){
            $this->success('发表评论成功，博主审核后将会展示！',url('index/article/index',['article_id'=>$param['article_id']]));
        }else{
            $this->error('发表评论失败！'.$comment->getError());
        }
    }
}