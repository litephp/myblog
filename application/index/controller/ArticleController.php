<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/9 0009
 * Time: 下午 20:21
 */

namespace app\index\controller;
use app\index\controller\base\BaseController;
use app\index\model\Article;
use app\index\model\Category;

class ArticleController extends BaseController {
    public function index() {
        if (empty(request()->route('article_id'))){
            ob_clean();
            throw new HttpResponseException(Response::create('非法访问！','html'));
        }
        $map = ['article_id'=>request()->route('article_id')];
        $article_now = Article::get($map);
        $article_now->setInc('click',1);
        $this->assign('article_now',$article_now);
        return $this->fetch();
    }
}