<?php

namespace app\index\controller;
use app\index\controller\base\BaseController;

/**
 * 首页控制器
 * Class IndexController
 * @package app\index\controller
 */
class IndexController extends BaseController
{
    public function index(){
        return $this->fetch();
    }
}
