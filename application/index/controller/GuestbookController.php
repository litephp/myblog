<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10 0010
 * Time: 上午 0:29
 */

namespace app\index\controller;
use app\index\controller\base\BaseController;
use app\index\model\Guestbook;
use think\Request;

class GuestbookController extends BaseController {
    public function index() {
        return $this->fetch();
    }
    public function insert(Request $request){
        $param = $request->only(['content','qq','email','link'],'post');
        $guestbook = new Guestbook();
        $res = $guestbook->validate('Guestbook.user_insert')->allowField(true)->save($param);
        if ($res !== false){
            $this->success('发表留言成功，博主审核后将会展示！',url('index/guestbook/index'));
        }else{
            $this->error('发表留言失败！'.$guestbook->getError());
        }
        return $this->fetch();
    }
}