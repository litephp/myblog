<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/9 0009
 * Time: 下午 18:59
 */

namespace app\index\widget;
use app\index\model\About;
use app\index\model\Article;
use app\index\model\Category;
use app\index\model\Comment;
use app\index\model\Flink;
use app\index\model\Guestbook;
use app\index\model\Project;
use app\index\model\Speak;
use app\index\model\Tags;
use jayfun\layuiPager\Layui;
use think\exception\HttpResponseException;
use think\Response;
use think\View;

class Blog {
    /**
     * 分类列表插件
     * @return string
     */
    public function category_list(){
        $cate_list = Category::all();
        $view = new View();
        $view->assign('cate_list',$cate_list);
        return $view->fetch('widget/blog/category_list');
    }

    /**
     * 文章列表插件
     * @return string
     */
    public function article_list(){
        $map= [];
        if (!empty(request()->route('cate_id'))){
            $map = ['cate_id'=>request()->route('cate_id')];
        }
        $art_list = Article::where($map)->order('article_id desc')->paginate();
        $view = new View();
        $view->assign('art_list',$art_list);
        return $view->fetch('widget/blog/article_list');
    }

    /**
     * 标签文章列表插件
     * @return string
     */
    public function article_list_tag(){
        if (!empty(request()->route('tag_id'))){
            $map = ['tag_id'=>request()->route('tag_id')];
			/*使用子查询*/
			$art_list = Article::where([
				'article_id'=>['IN',function($query) use ($map){
					$query->table('__TAGS_MAP__')->where($map)->group('tag_id')->field('GROUP_CONCAT(`article_id`)');
				}]
			])->order('article_id desc')->paginate();
        }else{
			$art_list = Article::where(['tags'=>''])->order('article_id desc')->paginate();
		}

        $view = new View();
        $view->assign('art_list',$art_list);
        return $view->fetch('widget/blog/article_list_tag');
    }

	/**
	 * 标签云插件
	 */
	public function tags_cloud() {
		$view = new View();
		$tags= Tags::all(function($query){
			$query->field('tag_id,name');
		});
		$view->assign('tags',$tags);
		return $view->fetch('widget/blog/tags_cloud');
	}
    /**
     * 文章插件
     * @return string
     */
    public function article(){

        $map = ['article_id'=>request()->route('article_id')];
        $article = Article::get($map);
        $view = new View();
        $view->assign('article',$article);
        return $view->fetch('widget/blog/article');
    }

    /**
     * 文章评论插件
     * @return string
     */
    public function comment(){
        $map= [];
        if (!empty(request()->route('article_id'))){
            $map = ['article_id'=>request()->route('article_id')];
        }
        $comment_list = Comment::where($map)->where(['status'=>1])->order('id desc')->paginate();
        $view = new View();
        $view->assign('comment_list',$comment_list);
        return $view->fetch('widget/blog/comment');
    }

    /**
     * 个人资料插件
     * @return string
     */
    public function profile(){
        $profile = About::get(1);
        $view = new View();
        $view->assign('profile',$profile);
        return $view->fetch('widget/blog/profile');
    }

    /**
     * 版权信息插件
     * @return string
     */
    public function copyright(){
        $view = new View();
        return $view->fetch('widget/blog/copyright');
    }

    /**
     * 顶部导航
     * @return string
     */
    public function nav(){
        $view = new View();
        return $view->fetch('widget/blog/nav');
    }

    /**
     * 移动端测导航
     * @return string
     */
    public function nav_side(){
        $view = new View();
        return $view->fetch('widget/blog/nav_side');
    }

    /**
     * 项目插件
     * @return string
     */
    public function project(){
        $project_list = Project::all(function($query){
            $query->order('id desc');
        });
        $view = new View();
        $view->assign('project_list',$project_list);
        return $view->fetch('widget/blog/project');
    }

    /**
     * 友情链接插件
     * @return string
     */
    public function flink(){
        $flink_list = Flink::all(['status'=>1]);
        $view = new View();
        $view->assign('flink_list',$flink_list);
        return $view->fetch('widget/blog/flink');
    }

    /**
     * 说说插件
     * @return string
     */
    public function speak(){
        $speak_list = Speak::all(function($query){
            $query->order('id desc');
        });
        $view = new View();
        $view->assign('speak_list',$speak_list);
        return $view->fetch('widget/blog/speak');
    }

    /**
     * 首页顶部 最新说说
     * @return string
     */
    public function latest_speak(){
        $speak = Speak::get(function ($query){
            $query->field('content')->order('create_time desc');
        });
        $view = new View();
        $view->assign('speak',$speak);
        return $view->fetch('widget/blog/latest_speak');
    }

    /**
     * 留言板插件
     * @return string
     */
    public function guestbook(){
        $guestbook_list = Guestbook::where(['status'=>1])->order('id desc')->paginate();
        $view = new View();
        $view->assign('guestbook_list',$guestbook_list);
        return $view->fetch('widget/blog/guestbook');
    }

    /**
     * 关于我插件
     * @return string
     */
    public function about(){
        $profile = About::get(1);
        $view = new View();
        $view->assign('profile',$profile);
        return $view->fetch('widget/blog/about');
    }
}