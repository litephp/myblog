<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10 0010
 * Time: 上午 11:18
 */

namespace app\index\model;
use think\Model;

class Article extends Model {
    public function getTagListAttr($value,$data){
        if (empty($data['tags'])) return null;
        return Tags::all(['tag_id'=>['IN',$data['tags']]]);
    }
}