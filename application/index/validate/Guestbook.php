<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10 0010
 * Time: 下午 12:49
 */

namespace app\index\validate;
use think\Validate;

class Guestbook extends Validate {
    protected $message  =   [
        '__token__.token'=>'访问异常请刷新后重试！'
    ];
    protected $scene = [
        'user_insert'  =>  ['captcha', 'qq', 'email', 'link', 'content','__token__'],
    ];
    protected $rule = [
        '__token__|令牌'  =>  'require|token',
        'captcha|验证码' =>  'require|captcha',
        'qq|QQ号'  =>  'require|number|length:5,11',
        'email|电子邮箱'  =>  'require|email',
        'link|个人主页'  =>  'require|url',
        'content|留言'  =>  'require',
    ];
}