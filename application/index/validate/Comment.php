<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10 0010
 * Time: 下午 12:49
 */

namespace app\index\validate;
use think\Validate;

class Comment extends Validate {
    protected $message  =   [
        '__token__.token'=>'访问异常请刷新后重试！'
    ];
    protected $scene = [
        'user_insert'  =>  ['captcha', 'article_id', 'link', 'content','__token__'],
    ];
    protected $rule = [
        '__token__|令牌'  =>  'require|token',
        'captcha|验证码' =>  'require|captcha',
        'article_id|文章'  =>  'require|number|egt:0',
        'link|个人主页'  =>  'require|url',
        'content|评论'  =>  'require',
    ];
}