<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/13 0013
 * Time: 上午 11:14
 */

namespace app\user\controller;
use think\Controller;
use think\exception\HttpResponseException;
use think\Session;

class ServiceController extends Controller {
    public function login(){
        return $this->fetch();
    }

    public function dologin(){
        Session::set('user_id',1);
        $response = redirect('user/index/index');
        throw new HttpResponseException($response);
    }
}