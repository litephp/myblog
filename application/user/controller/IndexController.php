<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/13 0013
 * Time: 上午 11:15
 */

namespace app\user\controller;


use think\Controller;

class IndexController extends Controller {
    public function index(){
        return $this->fetch();
    }
}