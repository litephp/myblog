<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/13 0013
 * Time: 上午 11:18
 */
namespace app\user\behavior;
use think\exception\HttpResponseException;
use think\Response;
use think\Session;

/**
 * 登录检测
 * Class SignDetection
 * @package app\user\behavior
 */
class SignDetection {
    public function run(&$param){
        $request = $param;
        if ($request->controller() === 'Service'){
            return;
        }
        if (!Session::has('user_id')){
            $response = redirect('user/service/login');
            throw new HttpResponseException($response);
            //var_dump($response);die();
        }
    }
}