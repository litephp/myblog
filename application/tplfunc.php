<?php
/**
 * 自定义函数
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/6 0006
 * Time: 下午 15:19
 */
/**
 * 模版中长字符串展示
 * @param $string
 * @param $start
 * @param $length
 * @param $encoding
 * @return string
 */
function t_mbsubstr($string, $start, $length, $encoding){
    $return = mb_substr($string, $start, $length, $encoding);
    if (strlen($string)>($start+$length))$return .= '...';
    return $return;
}

/**
 * 根据等级增加缩进量
 * @param $level
 */
function t_indent_by_level($level){
    return '|'.str_repeat('&#12288',intval($level)-1).'|—';
}
